#ifndef _BUNDLE_DEFS_H_
#define _BUNDLE_DEFS_H_

#if defined(_WIN32)
#include <windef.h>
#define int8 INT8
#define int16 INT16
#define int32 INT32
#define int64 INT64
#define uint8 UINT8
#define uint16 UINT16
#define uint32 UINT32
#define uint64 UINT64

#else /* defined(_WIN32) */
#include <stdint.h>
#define int8 int8_t
#define int16 int16_t
#define int32 int32_t
#define int64 int64_t
#define uint8 uint8_t
#define uint16 uint16_t
#define uint32 uint32_t
#define uint64 uint64_t

#endif /* defined(_WIN32) */

#endif /* _BUNDLE_DEFS_H_ */
