#include <pipe.h>

_Static_assert(sizeof(Pipe) == 32, "Pipe structure must be 32 bytes");
