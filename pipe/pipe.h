/* Pipes facilitate bidirectional inter-process communication (NOT inter-thread communication). */
#ifndef _LIBPIPE_PIPE_H_
#define _LIBPIPE_PIPE_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#if defined(_WIN32)
#include <windows.h>
#include <tchar.h>
#else /* defined(_WIN32) */
#include <sys/types.h>
#include <sys/wait.h>
#endif /* defined(_WIN32) */

#include <bundle/bundle.h>

typedef union Pipe {
  int64 _normalize_size_to_32_bytes[4];
  struct {
#if defined(_WIN32)
    HANDLE ihandle;
    HANDLE ohandle;
#else /* defined(_WIN32) */
    int ofds[2];
    int ifds[2];
    pid_t creator;
#endif
  };
} Pipe;

int initPipe(Pipe *result);

#endif /* _LIBPIPE_PIPE_H_ */
