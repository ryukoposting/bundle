#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include <bundle/bundle.h>
#include <process/process.h>
#include <process/streams.h>

#define BUFSZ 1024

char buf[BUFSZ] = {};

int
main()
{
  int stat;
  
  char *argv[] = {
    "./test.exe",
    0
  };
  int argc = 1;
  
  printf("%d\n", sizeof(Process));
  
  Process proc;
  ProcStream stream;
  
  if (stat = initProcess(&proc, argc, argv)) {
    fprintf(stderr, "error occurred while initializing process: %d\n", stat);
    exit(EXIT_FAILURE);
  }
  
  if (stat = initProcStream(&stream, &proc)) {
    fprintf(stderr, "error occurred while initializing procstream: %d\n", stat);
    exit(EXIT_FAILURE);
  }
  
  if (stat = startProcess(&proc)) {
    fprintf(stderr, "failed to start process (%d)\n", GetLastError());
    exit(EXIT_FAILURE);
  }
  
  int done;
  int i = 0;
  while (!(done = processDone(&proc, &stat))) {
    long int nbytes = 0;
    if ((i <= 10) && (!readStdout(&stream, &buf, BUFSZ, &nbytes))) {
      printf("child said: %s\n", buf);
    }
    ++i;
  }
  
  printf("process exited with code %d\n", stat);
  
  freeProcess(&proc);
}
