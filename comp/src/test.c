#include <unistd.h>
#include <stdio.h>

#if defined(_WIN32)
#include <windows.h>
#include <tchar.h>
#else /* defined(_WIN32) */
#include <sys/types.h>
#include <sys/wait.h>
#endif /* defined(_WIN32) */

int
main()
{
  HANDLE hstdin = GetStdHandle(STD_INPUT_HANDLE);
  HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
  printf("testing a write to stdout\n");
  sleep(5);
  printf("another write to stdout\n");
  return 0;
}
