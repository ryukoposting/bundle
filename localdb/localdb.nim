import os, ospaths, marshal, json, strformat, locks, times, md5, osproc

when not defined(windows):
  from posix import getpid

type
  LocalDb* = ref object
    repoName*: string

var
  skLock: Lock
  skInitialized = false
  sk: string

initLock skLock

when defined windows:
  echo getEnv("LOCALAPPDATA")
  echo getEnv("TEMP")


proc userName(): string =
  when defined windows:
    getEnv("USERNAME")
  else:
    getEnv("USER")


proc getProcessId*(): int =
  when defined(windows):
    proc getCurrentProcessId(): int32 {.stdcall, dynlib: "kernel32",
                                        importc: "GetCurrentProcessId".}
    result = getCurrentProcessId()
  else:
    result = getpid()


proc sessionKey(): string =
  acquire skLock
  if not skInitialized:
    let digest = fmt"""{now().format("yyyyMMddhhmmssffffff")}{userName()}{getCurrentDir()}{getProcessId()}"""
    sk = $toMd5(digest)
    skInitialized = true
  release skLock
  result = sk


proc dotStringGen(jn: JsonNode, key = ""): iterator: string =
  return iterator: string =
    case jn.kind:
    of JObject:
      for k, v in jn.pairs:
        let ds = dotStringGen(v, k)
        for d in ds():
          yield fmt"{key}.{d}"
    else:
      yield key


iterator dotStrings(jn: JsonNode): string =
  let ds = dotStringGen(jn)
  for d in ds():
    yield d[1..d.high]

echo sessionKey()


