#ifndef _GETLINE_H_
#define _GETLINE_H_


ssize_t __STATIC__getline(char **buf, size_t *bufsiz, FILE *fp);
ssize_t __STATIC__getdelim(char **buf, size_t *bufsiz, int delimiter, FILE *fp);

/* if the libc being used is POSIX 2009 or newer, it already has getline and getdelim. */
#if _POSIX_C_SOURCE < 200809L || (!defined(_POSIX_C_SOURCE))
#define getdelim(b, c, d, f) __STATIC__getdelim(b, c, d, f)
#define getline(b, c, f) __STATIC__getline(b, c, f)
#endif /* _POSIX_C_SOURCE */

#endif
