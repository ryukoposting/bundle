#ifndef _LIBPROCESS_STREAMS_H_
#define _LIBPROCESS_STREAMS_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#if defined(_WIN32)
#include <windows.h>
#include <tchar.h>
#else /* defined(_WIN32) */
#include <sys/types.h>
#include <sys/wait.h>
#endif /* defined(_WIN32) */

#include <bundle/bundle.h>

#include <process.h>

typedef struct ProcStream {
#if defined(_WIN32)
  SECURITY_ATTRIBUTES sa;
  HANDLE inwr;
  HANDLE inrd;
  HANDLE outwr;
  HANDLE outrd;
  HANDLE errwr;
  HANDLE errrd;
#else /* defined(_WIN32) */
  int ipipe[2];
  int opipe[2];
#endif /* defined(_WIN32) */
} ProcStream;

int initProcStream(ProcStream *result, Process *proc);

int readStdout(ProcStream *ps, char *buf[], long int bufsz, long int *nread);

#endif /* _LIBPROCESS_PROCESS_H_ */
