#ifndef _LIBPROCESS_PROCESS_H_
#define _LIBPROCESS_PROCESS_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#if defined(_WIN32)
#include <windows.h>
#include <tchar.h>
#else /* defined(_WIN32) */
#include <sys/types.h>
#include <sys/wait.h>
#endif /* defined(_WIN32) */

#include <bundle/bundle.h>

typedef union Process {
  int64 _normalize_size_to_64_bytes[8];
  struct {
    void *alloc;
#if defined(_WIN32)
    STARTUPINFO *si;
    PROCESS_INFORMATION pi;
    BOOL inherithandles;
    char *appname;
    char *cmdline;
#else
    char **argv;
    int argc;
    pid_t pid;
#endif
  };
} Process;

/* initializes the Process structure. returns -1 if a memory allocation fails.
   returns -2 if argv contains a null in an index before the value of argc.
   returns -3 if argv is null, result is null, or argv[argc] is not null.
   argv is a null-terminated array of null-terminated strings. The first member
   of argv is expected to be the program to be executed. 
   
   You must call initProcess on a Process structure exactly once before 
   calling startProcess() on it.
   
   Calling initProcess twice on the same Process structure (without calling
   freeProcess() on it in between calls) results in undefined behavior. */
int initProcess(Process *result, int argc, char *argv[]);

/* starts the Process. This should only be called on a Process structure that
   was initialized by initProcess. startProcess can be called multiple times on
   the same Process structure, provided that the Process has completed before
   each call to startProcess. */
int startProcess(Process *p);

/* returns 1 if process completed. returns 0 if process is still active. if the
   process completed, the integer pointed to by status will be set to the exit
   code of the process. returns -1 on error. */
int processDone(Process *p, int *status);

/* free the resources stored inside p. Calling this function on a Process that
   hass not been initialized with initProcess() results in undefined behavior.
   If you initialize a Process, then free it, you can reuse that structure by
   calling initProcess */
void freeProcess(Process *p);

#endif /* _LIBPROCESS_PROCESS_H_ */
