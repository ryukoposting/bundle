#include <streams.h>

// _Static_assert(sizeof(ProcStream) == 32, "ProcStream structure must be 32 bytes");

int
initProcStream(ProcStream *result, Process *proc)
{
  int status = 0;
  if ((!result) || (!proc) || (!proc->alloc))
    status = -2;
  
  memset(result, 0, sizeof(ProcStream));
  
#if defined(_WIN32)
  result->sa.nLength = sizeof(SECURITY_ATTRIBUTES);
  result->sa.bInheritHandle = TRUE;
  result->sa.lpSecurityDescriptor = NULL;
  
  if (status == 0) {
    int res = CreatePipe(&result->outrd, &result->outwr, &result->sa, 0);
    if (!res)
      status = -1;
  }
  if (status == 0) {
    int res = SetHandleInformation(result->outrd, HANDLE_FLAG_INHERIT, 0);
    if (!res)
      status = -1;
  }
  if (status == 0) {
    int res = CreatePipe(&result->errrd, &result->errwr, &result->sa, 0);
    if (!res)
      status = -1;
  }
  if (status == 0) {
    int res = SetHandleInformation(result->errrd, HANDLE_FLAG_INHERIT, 0);
    if (!res)
      status = -1;
  }
  if (status == 0) {
    int res = CreatePipe(&result->inwr, &result->inwr, &result->sa, 0);
    if (!res)
      status = -1;
  }
  if (status == 0) {
    int res = SetHandleInformation(result->inwr, HANDLE_FLAG_INHERIT, 0);
    if (!res)
      status = -1;
  }
  
  proc->si->hStdError = result->errwr;
  proc->si->hStdOutput = result->outwr;
  proc->si->hStdInput = result->inrd;
  
  if (status == 0) {
    proc->si->dwFlags |= STARTF_USESTDHANDLES;
    proc->inherithandles = TRUE;
  }
  
#else /* defined(_WIN32) */
  if (status == 0) {
    int res = pipe(result->ipipe);
    if (res < 0)
      status = -1;
  }
  
  if (status == 0) {
    int res = pipe(result->opipe);
    if (res < 0) {
      close(result->ipipe[0]);
      close(result->ipipe[1]);
      status = -1;
    }
  }
#endif /* defined(_WIN32) */
  
  return status;
}

int
readStdout(ProcStream *ps, char *buf[], long int bufsz, long int *nread)
{
  int result = 0;
  
#if defined(_WIN32)
  int res = ReadFile(ps->outrd, *buf, bufsz, nread, NULL);
  if (!res) {
    printf("got error code=%d\n", GetLastError());
    result = -1;
  }
#else /* defined(_WIN32) */
  
#endif /* defined(_WIN32) */
  
  return result;
}
