#include <process.h>

_Static_assert(sizeof(Process) == 64, "Process structure must be 64 bytes");

int
initProcess(Process *result, int argc, char *argv[])
{ /* initializes the Process structure. returns -1 if a memory allocation fails.
   returns -2 if argv contains a null in an index before the value of argc.
   returns -3 if argv is null, result is null, or argv[argc] is not null.
   argv is a null-terminated array of null-terminated strings. */
  int status = 0;
  if ((!argv) || (!result) || argv[argc])
    return -3;
  
  memset(result, 0, sizeof(Process));
  
  /* STEP 1: determine the number of bytes to be allocated */
  int poolsz = 0;
  int8 *pool = NULL;
  int *slens = calloc(argc, sizeof(int)); /* on heap to mitigate risk of stack overflow */
#if defined(_WIN32)
  result->inherithandles = FALSE;
  
  poolsz += sizeof(STARTUPINFO);
  for (int i = 0; (status == 0) && i < argc; ++i) {
    char *arg = argv[i];
    if (!arg) {
      status = -2;
      break;
    }
    slens[i] += strlen(arg);
    poolsz += slens[i] + 1;
  }
  poolsz += slens[0] + 1;
  
#else /* defined(_WIN32) */
  result->argc = argc;
  
  if (!slens)
    status = -1;
  
  /* number of bytes to deep copy argv */
  poolsz += (sizeof(char*) * (argc + 1));
  
  for (int i = 0; (status == 0) && i < argc; ++i) {
    char *arg = argv[i];
    if (!arg) {
      status = -2;
      break;
    }
    slens[i] = strlen(arg);
    poolsz += slens[i] + 1;
  }
#endif /* defined(_WIN32) */
  
  /* STEP 2: request however many bytes are needed */
  if (status == 0) {
    pool = malloc(poolsz);
    if (!pool)
      status = -1;
    else
      result->alloc = pool;
  }

  /* STEP 3: fill in the pool */
#if defined(_WIN32)
  if (status == 0) {
    result->si = (STARTUPINFO*)pool;
    memset(result->si, 0, sizeof(STARTUPINFO));
    result->si->cb = sizeof(STARTUPINFO);
    pool += sizeof(STARTUPINFO);
    
    
    result->appname = strncpy(pool, argv[0], slens[0] + 1);
    pool += slens[0] + 1;
    
    result->cmdline = pool;
    
    for(int i = 0; i < argc; ++i) {
      
      strncpy(pool, argv[i], slens[i]);
      pool[slens[i]] = ' ';
      pool += slens[i] + 1;
    }
    
    pool[0] = '\0';
  }

#else /* defined(_WIN32) */
  if (status == 0) {
    result->argv = (char**)pool;
    pool += (sizeof(char*) * (argc + 1));
    for (int i = 0; status == 0 && i < argc; ++i) {
      result->argv[i] = pool;
      strncpy(result->argv[i], argv[i], slens[i] + 1);
      pool += slens[i] + 1;
    }
    
    result->argv[argc] = NULL;
  }
#endif /* defined(_WIN32) */
  
  if (slens)
    free(slens);
  
  return status;
}

int
startProcess(Process *p)
{
  int result = 0;
  assert(p);
  assert(p->alloc);
  
#if defined(_WIN32)
  BOOL cpresult = CreateProcess(
//     p->appname,
    NULL,
    p->cmdline,
    NULL,
    NULL,
    p->inherithandles,
    0,
    NULL,
    NULL,
    p->si,
    &(p->pi));
  if (!cpresult)
    result = -1;
#else /* defined(_WIN32) */
  pid_t pid = fork();
  if (pid < 0)
    result = -1;
  else if (pid > 0)
    p->pid = pid;
  else {
    execvp(p->argv[0], p->argv);
    exit(EXIT_FAILURE);
  }
#endif /* defined(_WIN32) */
  
  return result;
}

int
processDone(Process *p, int *status)
{ /* returns 1 if process finished. returns 0 if process is still active. if the
   process completed, the integer pointed to by status will be set to the exit
   code of the process. returns -1 on error. */
  int result = 0;
  assert(p);
  assert(p->alloc);
  assert(status);
  
#if defined(_WIN32)
  DWORD stat;
  BOOL res = GetExitCodeProcess(p->pi.hProcess, &stat);
  if (!res)
    result = -1;
  else if (res && stat == STILL_ACTIVE)
    result = 0;
  else {
    result = 1;
    *status = stat;
  }
#else /* defined(_WIN32) */
  pid_t wp = waitpid(p->pid, status, WNOHANG);
  if (wp == -1)
    result = -1;
  else if (wp == p->pid)
    result = 1;
  else
    result = 0;
#endif /* defined(_WIN32) */
  
  return result;
}

void
freeProcess(Process *p)
{
  assert(p);
  
  if (p->alloc)
    free(p->alloc);
  
  memset(p, 0, sizeof(Process));
}
